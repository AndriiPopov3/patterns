export const texts = [
  "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean egestas mi sed sem eleifend, non vulputate magna dapibus.",
  "Donec luctus dui vel volutpat porta. Donec faucibus lorem est, id condimentum risus dignissim sed. Fusce non nibh ornare, molestie nulla eget, hendrerit massa.",
  "Phasellus vel sem lobortis lorem venenatis sagittis sed sit amet erat. Aenean quis hendrerit ante, sed maximus massa. Nam vitae faucibus est. Maecenas mollis scelerisque justo, ac accumsan justo fringilla ac. Mauris sit amet rutrum velit. Donec accumsan lobortis dui, a varius mauris consectetur commodo.",
  "In tincidunt, lorem a scelerisque mollis, dui nisi lobortis dui, id cursus nunc ex id augue. Mauris quis dapibus lorem. Etiam a orci ultricies, aliquam felis volutpat, hendrerit lacus. Sed feugiat elit ex, id laoreet dui elementum in. Vivamus pharetra ut tortor eu ornare. Fusce porttitor tristique dictum.",
  "Sed consectetur massa in diam volutpat tincidunt. Aenean facilisis mi felis, sit amet maximus metus posuere at. Curabitur quis diam at quam pellentesque sollicitudin. Donec orci ex, euismod in consequat nec, scelerisque quis eros.",
  "Praesent nec semper quam. Aliquam et vulputate tortor. Nam maximus leo erat. Suspendisse in neque quis sapien sagittis aliquam. Etiam mollis malesuada massa a ullamcorper. Sed odio lectus, rutrum a placerat ac, tincidunt eu dolor.",
  "Praesent rutrum nunc mi, sed interdum ligula tempor ac. Donec auctor ipsum ligula, nec tempus erat mattis in. Fusce quis sagittis sapien. Pellentesque at sem sit amet nulla pretium lacinia vitae ac eros. Donec vel ipsum ac mauris gravida eleifend. Morbi ut maximus felis."
];

export default { texts };
