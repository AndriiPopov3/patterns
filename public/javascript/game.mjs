import { createElement, addClass } from "./helper.mjs";
import { createRoomInterface, updateRoomInterface } from "./room-interface-creators.mjs";
import { updateRooms, leaveRoomDone } from "./update-rooms.mjs";
import { userReadyDone, userNotReadyDone, userReadyDoneRoom, userNotReadyDoneRoom } from "./user-ready-updaters.mjs";
import { renderBot, sendComment } from './bot.mjs';

export const joinRoom = room_name => {
  socket.emit("JOIN_ROOM", room_name);
}

export const leaveRoom = room_name => {
  socket.emit("LEAVE_ROOM", room_name);
}

export const userReady = (room, user) => {
  socket.emit("USER_READY", {room, user});
}

export const userNotReady = (room, user) => {
  socket.emit("USER_NOT_READY", {room, user});
}

const username = sessionStorage.getItem("username");

const socket = io("http://localhost:3002/game", { query: { username } });

const addRoom = () => {
  const room_name = prompt("enter room name:", '');
  if (room_name) {
    socket.emit("CREATE_ROOM", room_name);
  }
};

const addRoomButton = document.getElementById("add-room-btn");
addRoomButton.addEventListener("click", addRoom);

// global flag to stop timer once the game is over
let isGameOn = false;

if (!username) {
  window.location.replace("/login");
}

if (username) {
  socket.emit("USERNAME_UNIQUE", username);
}

const usernameExists = username => {
  alert(`Error: username ${username} exists`);
  sessionStorage.removeItem("username");
  window.location.replace("/login");
};

const onExistingRoomName = room => {
  alert(`Error: Room name '${room}' already exists`);
}

const onMaxUsers = room => {
  alert(`Room '${room}' already has maximum number of players`);
}

const areUsersReady = room_data => {
  const greenIcons = document.getElementsByClassName("ready-status-green");
  if(greenIcons.length === room_data.roomUsersNum) {
    beginCountdown(room_data.room);
  }
}

const beginCountdown = room => {
  socket.emit("START_COUNTDOWN", {room});
  socket.emit("ACTIVATE_BOT", room);
  socket.emit("BOT_RANDOM_LINE", room);
}

const beginCountdownDone = room_data => {
  socket.emit("START_GAME", room_data);
}

const beginGame = room_data => {
  const readyButton = document.getElementById("ready-btn");
  const rightContainer = document.getElementById("right-container");
  const leaveRoomButton = document.getElementById("quit-room-btn");
  addClass(readyButton, "display-none");
  addClass(leaveRoomButton, "display-none");
  const countdown = createElement({
    tagName: "div",
    attributes: {
      id: "timer"
    }
  });
  rightContainer.append(countdown);
  let gameText = "";
  fetch(`http://localhost:3002/game/texts/${room_data.text}`)
  .then((response) => {
    return response.json();
  })
  .then((data) => {
    gameText = data;
  });
  let countdownTime = new Date().getTime();
  countdownTime = countdownTime + (room_data.time * 1000);

  const pre_game_timer = function() {
    let now = new Date().getTime();
    let distance = countdownTime - now;
    document.getElementById("timer").innerHTML = Math.floor(distance / 1000);
    if (distance < 5000) {
      socket.emit("BOT_NAME_USERS", room_data.room);
    }

    if (distance < 1000) {
      clearTimeout(x);
      const rightContainer = document.getElementById("right-container");
      addClass(document.getElementById("timer"), "display-none");

      const textContainer = createElement({
        tagName: "div",
        attributes: {
          id: "text-container"
        }
      });
      for (let i = 0; i < gameText.text.length; i++) {
        let span = createElement({
          tagName: "span",
          className: "span"
        });
        span.innerText = gameText.text.slice(i, i+1);
        textContainer.append(span);
      }
      rightContainer.append(textContainer);
      socket.emit("BOT_RACE_START", room_data.room);
      startGame(room_data.gameTime, room_data.user, room_data.room);
    } else {
      setTimeout(pre_game_timer, 1000)
    }
  }
  const x = setTimeout(pre_game_timer, 1000);
}

const startGame = (time, user, room) => {
  isGameOn = true;
  document.addEventListener("keydown", typing, false);
  const spans = document.querySelectorAll(".span");
  spans[0].classList.add("nextChar");
  const rightContainer = document.getElementById("right-container");
  function typing(event) {
    let name = event.key;
    for (let i = 0; i < spans.length; i++) {
      if (spans[i].innerHTML === name) {
        if (spans[i].classList.contains("bg")) { // if it already has class with the background color then check the next one
          continue;
        } else if (spans[i].classList.contains("bg") === false && spans[i-1] === undefined || spans[i-1].classList.contains("bg") !== false ) { 
          spans[i].classList.remove("nextChar");
          spans[i].classList.add("bg");
          if(i+1 !== spans.length) {
            spans[i+1].classList.add("nextChar");
          }
          socket.emit("UPDATE_PROGRESS_BARS", {room, user, current: i+1, full: spans.length, time});
          break;
        }
      }
    }
  };
  const gameCountdown = createElement({
    tagName: "div",
    attributes: {
      id: "game-timer"
    }
  });
  rightContainer.append(gameCountdown);
  let countdownTime = new Date().getTime();
  countdownTime = countdownTime + (time * 1000);

  // timer function
  const game_timer = function() {
    let now = new Date().getTime();
    let distance = countdownTime - now;
    document.getElementById("game-timer").innerHTML = Math.floor(distance / 1000) + " seconds left";

    if (distance < (time * 1000 - 2000) && distance > (time * 1000 - 4000) && isGameOn) {
      socket.emit("BOT_NAME_AFTER_START_LEADER", room);
    }

    if (Math.floor(distance % 30000) < 1010 && isGameOn) {
      socket.emit("BOT_ANNOUNCE_STATE", {room, distance});
    }

    if (distance < 1500) {
      clearTimeout(x);
      document.removeEventListener("keydown", typing, false);
      socket.emit("GAME_TIMEOUT", {
        room,
        user
      });
    }

    if (isGameOn === false) {
      clearTimeout(x);
      document.removeEventListener("keydown", typing, false);
    } else {
      setTimeout(game_timer, 1000);
    }
  }
  const x = setTimeout(game_timer, 1000);
}

const updateProgressBars = user_progress_data => {
  const userProgress = document.getElementsByClassName(`user-progress ${user_progress_data.user}`);
  userProgress[0].style.width = (user_progress_data.current === user_progress_data.full) ? "100%" : ((user_progress_data.current / user_progress_data.full) * 100) + "%" ;
  if (userProgress[0].style.width === "100%") {
    userProgress[0].classList.add("finished");
    const finisherNum = document.getElementsByClassName("finished").length;
    socket.emit("BOT_ANNOUNCE_FINISHER", { user: user_progress_data.user, rank: finisherNum, room: user_progress_data.room});
    const gameTimer = document.getElementById("game-timer").innerText;
    const timeLeft = +gameTimer.slice(0, gameTimer.length - 13);
    const timeUser = user_progress_data.time - timeLeft;
    socket.emit("ADD_FINISHER", { 
      user: user_progress_data.user, 
      rank: finisherNum, 
      room: user_progress_data.room, 
      time: timeUser
    });
  }
  socket.emit("WRITE_USER_PROGRESS", {
    user: user_progress_data.user,
    current: user_progress_data.current,
    full: user_progress_data.full,
    room: user_progress_data.room
  });
}

const finishGameDone = () => {
  isGameOn = false;
}

const timeoutGameDone = () => {
  isGameOn = false;
}

socket.on("USERNAME_EXISTS", usernameExists);
socket.on("UPDATE_ROOMS", updateRooms);
socket.on("LEAVE_ROOM_DONE", leaveRoomDone);
socket.on("CREATE_ROOM_INTERFACE", createRoomInterface);
socket.on("UPDATE_ROOM_INTERFACE", updateRoomInterface);
socket.on("ROOM_NAME_EXISTS", onExistingRoomName);
socket.on("ROOM_HAS_MAX_USERS", onMaxUsers);
socket.on("USER_READY_DONE", userReadyDone);
socket.on("USER_READY_DONE_ROOM", userReadyDoneRoom);
socket.on("USER_NOT_READY_DONE", userNotReadyDone);
socket.on("USER_NOT_READY_DONE_ROOM", userNotReadyDoneRoom);
socket.on("ARE_USERS_READY", areUsersReady);
socket.on("START_COUNTDOWN_DONE", beginCountdownDone);
socket.on("START_GAME_DONE", beginGame);
socket.on("UPDATE_PROGRESS_BARS_DONE", updateProgressBars);
socket.on("FINISH_GAME_DONE", finishGameDone);
socket.on("TIMEOUT_GAME_DONE", timeoutGameDone);

const activateBotDone = () => {
  renderBot();
}

const botSendMessage = line => {
  sendComment(line);
}

socket.on("ACTIVATE_BOT_DONE", activateBotDone);
socket.on("BOT_SEND_MESSAGE", botSendMessage);