import { createElement, removeClass, addClass } from "./helper.mjs";
import {leaveRoom, userReady} from "./game.mjs";

export const createRoomInterface = room_user => {
    const roomsPage = document.getElementById("rooms-page");
    addClass(roomsPage, "display-none");
    const gamePage = document.getElementById("game-page");
    removeClass(gamePage, "display-none");
    gamePage.innerHTML = "";
    // game page creation START
    const mainContainer = createElement({
      tagName: "div",
      attributes: {
        id: "main-container"
      }
    });
    const leftContainer = createElement({
      tagName: "div",
      attributes: {
        id: "left-container"
      }
    });
    const roomTitle = createElement({
      tagName: "h1"
    });
    roomTitle.innerText = room_user.room_name;
  
    const leaveRoomButton = createElement({
      tagName: "button",
      attributes: {
        id: "quit-room-btn"
      }
    });
    leaveRoomButton.innerText = "Leave Room";
    leaveRoomButton.addEventListener("click", () => leaveRoom(room_user.room_name));
  
    const userContainer = createElement({
      tagName: "div",
      attributes: {
        id: "user-container"
      }
    });
    
    const roomUsers = room_user.userList.map(user => createUserBlock(user))
  
    const rightContainer = createElement({
      tagName: "div",
      attributes: {
        id: "right-container"
      }
    });
  
    const readyButton = createElement({
      tagName: "button",
      attributes: {
        id: "ready-btn"
      }
    });
    readyButton.innerText = "READY";
    readyButton.addEventListener("click", () => userReady(room_user.room_name, room_user.username));
    userContainer.append(...roomUsers);
    leftContainer.append(roomTitle, leaveRoomButton, userContainer);
    rightContainer.append(readyButton);
    mainContainer.append(leftContainer, rightContainer);
    gamePage.append(mainContainer);
    // game page creation END
  }
  
const createUserBlock = user => {
    const userBlock = createElement({
      tagName: "div",
      className: "user-block"
    });
    const userInfo = createElement({
      tagName: "div",
      className: "user-block-info"
    });
    const userStatus = createElement({
      tagName: "div",
      className: "ready-status-red",
      attributes: {
        id: `${user}`
      }
    });
    const user_name = createElement({
      tagName: "h4",
      className: `user-name ${user}`
    })
    user_name.innerText = user;
    const progressBarIndicator = createElement({
      tagName: "div",
      className: `user-progress-indicator`
    });
    const progressBar = createElement({
      tagName: "div",
      className: `user-progress ${user}`
    });
    progressBarIndicator.append(progressBar);
    userInfo.append(userStatus, user_name);
    userBlock.append(userInfo, progressBarIndicator);
    return userBlock;
  }
  
export const updateRoomInterface = room_user => {
    const userContainer = document.getElementById("user-container");
    userContainer.innerHTML = "";
    const roomUsers = room_user.userList.map(user => createUserBlock(user));
    userContainer.append(...roomUsers);
  }