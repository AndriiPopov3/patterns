import { createElement, removeClass, addClass } from "./helper.mjs";
import { joinRoom } from "./game.mjs";

const roomList = document.getElementById("room-list");

const createRoomButton = (room, roomNum) => {
    const roomBlock = createElement({
      tagName: "div",
      className: "room",
      attributes: { id: room }
    });
  
    const roomTitle = createElement({
      tagName: "h3"
    });
    roomTitle.innerText = room;
  
    const userCount = createElement({
      tagName: "h5"
    });
    userCount.innerText = `${roomNum} user connected`;

    const joinRoomButton = createElement({
      tagName: "button",
      className: "join-btn"
    });
    joinRoomButton.innerText = "Join Room";
    joinRoomButton.addEventListener("click", () => joinRoom(room));
  
    roomBlock.append(roomTitle, userCount, joinRoomButton);
  
    return roomBlock;
};

export const updateRooms = rooms => {
  const allRooms = [];
  if (rooms) {
    for (let i=0; i < rooms[0]?.length; i++) {
      allRooms.push(createRoomButton(rooms[0][i], rooms[1][i]));
    }
    roomList.innerHTML = "";
    roomList.append(...allRooms);
  }
};

export const leaveRoomDone = rooms => {
  const roomsPage = document.getElementById("rooms-page");
  removeClass(roomsPage, "display-none");
  const gamePage = document.getElementById("game-page");
  addClass(gamePage, "display-none");
  updateRooms(rooms);
}