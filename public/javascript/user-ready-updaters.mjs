import { createElement } from "./helper.mjs";
import {userNotReady, userReady} from "./game.mjs";

export const userReadyDone = obj => {
    document.getElementById(obj.user).setAttribute("class", "ready-status-green");
    const rightContainer = document.getElementById("right-container");
    rightContainer.innerHTML = "";
    const readyButton = createElement({
      tagName: "button",
      attributes: {
        id: "ready-btn"
      }
    });
    readyButton.innerText = "NOT READY";
    readyButton.addEventListener("click", () => userNotReady(obj.room, obj.user));
    rightContainer.append(readyButton);
  }
  
export const userNotReadyDone = obj => {
    document.getElementById(obj.user).setAttribute("class", "ready-status-red");
    const rightContainer = document.getElementById("right-container");
    rightContainer.innerHTML = "";
    const readyButton = createElement({
      tagName: "button",
      attributes: {
        id: "ready-btn"
      }
    });
    readyButton.innerText = "READY";
    readyButton.addEventListener("click", () => userReady(obj.room, obj.user));
    rightContainer.append(readyButton);
}

export const userReadyDoneRoom = user => {
  document.getElementById(user.user).setAttribute("class", "ready-status-green");
}
  
export const userNotReadyDoneRoom = user => {
  document.getElementById(user.user).setAttribute("class", "ready-status-red");
}