import { BotComment } from "./bot/BotComment";

// ENTIRE LINE BUILDING LOGIC IS DONE UNDER BOT CLASS INSTANCE
export class Bot {    
    createComment (type) { // FACADE
      const comment = new BotComment().create(type);
      return comment;
    }
}
