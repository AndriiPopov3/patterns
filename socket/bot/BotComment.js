import { Greeting } from "./greeting/greeting";
import { UserList } from "./userList/userList";
import { RaceStart } from "./raceStart/raceStart";
import { AfterStartLeader } from "./afterStartLeader/afterStartLeader";
import { Random } from "./random/random";
import { AnnounceState } from "./announceState/announceState";
import { AnnounceFinisher } from "./announceFinisher/announceFinisher";
import { FinalResults } from "./finalResults/finalResults";
import { CloseFinal } from "./closeFinal/closeFinal";
import { FinishLine } from "./finishLine/finishLine";
import { GameTimeout } from "./gameTimeout/gameTimeout";

export class BotComment { // FACTORY
    create (type) {
        let comment;
        if (type === 'greeting') {
            comment = new Greeting();
        } else if (type === 'userList') {
            comment = new UserList();
        } else if (type === 'raceStart') {
            comment = new RaceStart();
        } else if (type === 'after-start-leader') {
            comment = new AfterStartLeader();
        } else if (type === 'random') {
            comment = new Random();
        } else if (type === 'announce-state') {
            comment = new AnnounceState();
        } else if (type === 'announce-finisher') {
            comment = new AnnounceFinisher();
        } else if (type === 'final-results') {
            comment = new FinalResults();
        } else if (type === 'close-final') {
            comment = new CloseFinal();
        } else if (type === 'finish-line') {
            comment = new FinishLine();
        } else if (type === 'game-timeout') {
            comment = new GameTimeout();
        }
        comment.type = type;
        comment.say = function () {
            return comment.line;
        }
        return comment;
    }  
}