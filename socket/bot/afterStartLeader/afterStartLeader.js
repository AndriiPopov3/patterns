import * as _ from 'lodash';
import { Helper } from "../helper/helper";
import { bot_lines } from "../../../bot-lines";

export class AfterStartLeader {
    constructor () {
        this.line = bot_lines.AFTER_START_LEADER
    }

    buildAfterStartLeaderLine (userProgress) {
        const helper = new Helper();
        let phrase = this.line;
        const players = Object.keys(userProgress);
        const progress = Object.values(userProgress);
        if(players.length === 0) {
            phrase = phrase + bot_lines.AFTER_START_LEADER_NONE;
        } else {
            if (players.length === 1) {
                phrase = phrase + `${players[0]}! ` + bot_lines.AFTER_START_LEADER_SOLO;
            } else {
                const createArray = _.curry(helper.createRunnerUpsArray);
                let runnerUps = createArray(players)(progress);
                runnerUps = helper.sortWinnersArray(runnerUps);
                phrase = phrase + `${runnerUps[0].user}! Який стрімкий початок! `;
            }
        }
        return phrase;
      }
}