import { bot_lines } from "../../../bot-lines";

export class AnnounceFinisher {
    constructor () {
      this.line = bot_lines.ANNOUNCE_FINISHER;
    }

    buildAnnounceFinisherLine(user, rank) {
      let phrase = this.line;
      phrase = `${user} на місці ${rank} ` + phrase;
      return phrase;
    }
}