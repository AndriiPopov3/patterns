import * as _ from 'lodash';
import { Helper } from "../helper/helper";
import { bot_lines } from "../../../bot-lines";

export class AnnounceState {
    constructor () {
        this.line = bot_lines.ANNOUNCE_STATE
    }

    buildAnnounceStateLine (userProgress, timeToEnd) {
        const helper = new Helper();
        let phrase = this.line;
        const players = Object.keys(userProgress);
        const progress = Object.values(userProgress);
        if(players.length === 0) {
            phrase = phrase + bot_lines.ANNOUNCE_STATE_NONE;
        } else {
            if (players.length === 1) {
                phrase = phrase + ` першим йде ${players[0]}! ` + bot_lines.ANNOUNCE_STATE_SOLO + `Між іншим, до кінця змагання лишилось ${timeToEnd} секунд!`;
            } else {
                const createArray = _.curry(helper.createRunnerUpsArray);
                let runnerUps = createArray(players)(progress);
                if (players.length === 2) {
                    runnerUps = helper.sortWinnersArray(runnerUps);
                    phrase = phrase + ` першим йде ${runnerUps[0].user}! Яка неймовірна швидкість! За ним з певним відставанням ${runnerUps[1].user}. Між іншим, до кінця змагання лишилось ${timeToEnd} секунд!`;
                } else {
                    runnerUps = helper.sortWinnersArray(runnerUps);
                    phrase = phrase + ` першим йде ${runnerUps[0].user}! Яка неймовірна швидкість! За ним з певним відставанням ${runnerUps[1].user}. Завершує трійку лідерів ${runnerUps[2].user}. Між іншим, до кінця змагання лишилось ${timeToEnd} секунд!`;
                }
            }
        }
        return phrase;
      }
}