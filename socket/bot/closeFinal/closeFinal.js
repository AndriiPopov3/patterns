import * as _ from 'lodash';
import { bot_lines } from "../../../bot-lines";
import { Helper } from "../helper/helper";

export class CloseFinal {
    constructor () {
        this.line = bot_lines.CLOSE_FINAL;
    }

    buildCloseFinalLine(userProgress) {
        const helper = new Helper();
        let phrase = this.line;
        const players = Object.keys(userProgress);
        const progress = Object.values(userProgress);
        if (players.length === 1) {
            phrase = phrase + bot_lines.CLOSE_FINAL_SOLO + `${players[0]} ` + `є єдиним учасником.`;
        } else {
            const createArray = _.curry(helper.createRunnerUpsArray);
            let runnerUps = createArray(players)(progress);
            if (players.length === 2) {
                runnerUps = helper.sortWinnersArray(runnerUps);
                phrase = phrase + bot_lines.CLOSE_FINAL_FIRST + `${runnerUps[0].user}! ` + bot_lines.CLOSE_FINAL_SECOND_DUO + `${runnerUps[1].user}. ` + bot_lines.CLOSE_FINAL_END; 
            } else {
                runnerUps = helper.sortWinnersArray(runnerUps);
                phrase = phrase + bot_lines.CLOSE_FINAL_FIRST + `${runnerUps[0].user}! ` + bot_lines.CLOSE_FINAL_SECOND + `${runnerUps[1].user} та ${runnerUps[2].user}. ` + bot_lines.CLOSE_FINAL_END;
            }
        }
        return phrase;
    }
    
}