import { bot_lines } from "../../../bot-lines";

export class FinalResults {
    constructor () {
        this.line = bot_lines.FINAL_RESULTS;
    }

    buildFinalResultsLine(winners) {
        let phrase = this.line;
        const winner = Object.values(winners);
        if (winner.length === 1) {
            phrase = phrase + `${winner[0][0]}! Дивно було б очікувати чогось іншого. Його час становить ${winner[0][1]} сек.`;
        }
        if (winner.length === 2) {
            phrase = phrase + `${winner[0][0]}! Його час становить ${winner[0][1]} сек. На другому місці ${winner[1][0]} з часом ${winner[1][1]} сек.`;
        }
        if ([3,4,5].includes(winner.length)) {
            phrase = phrase + `${winner[0][0]}! Його час становить ${winner[0][1]} сек. На другому місці ${winner[1][0]} з часом ${winner[1][1]} сек. Закриває трійку лідерів ${winner[2][0]} з часом ${winner[2][1]} сек.`;
        }
        return phrase;
    }
    
}