import * as _ from 'lodash';
import { bot_lines } from "../../../bot-lines";
import { Helper } from "../helper/helper";

export class FinishLine {
    constructor () {
        this.line = bot_lines.FINISH_LINE;
    }

    buildFinishLineLine(userProgress) {
        const helper = new Helper();
        let phrase = this.line;
        const players = Object.keys(userProgress);
        const progress = Object.values(userProgress);
        if (players.length === 1) {
            phrase = phrase + `${players[0]}`;
        } else {
            const createArray = _.curry(helper.createRunnerUpsArray);
            let runnerUps = createArray(players)(progress);
            if (players.length === 2) {
                runnerUps = helper.sortWinnersArray(runnerUps);
                phrase = phrase + `${runnerUps[0].user}! ` + bot_lines.FINISH_LINE_SECOND_DUO + `${runnerUps[1].user}. ` + bot_lines.FINISH_LINE_END; 
            } else {
                runnerUps = helper.sortWinnersArray(runnerUps);
                phrase = phrase + `${runnerUps[0].user}! ` + bot_lines.FINISH_LINE_SECOND + `${runnerUps[1].user} та ${runnerUps[2].user}. ` + bot_lines.FINISH_LINE_END;
            }
        }
        return phrase;
    }
    
}