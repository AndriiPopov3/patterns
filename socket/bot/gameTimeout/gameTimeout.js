import * as _ from 'lodash';
import { Helper } from "../helper/helper";
import { bot_lines } from "../../../bot-lines";

export class GameTimeout {
    constructor () {
        this.line = bot_lines.GAME_TIMEOUT
    }

    buildGameTimeoutLine (userProgress, winnerList) { 
        const helper = new Helper();
        let phrase = this.line;
        const players = Object.keys(userProgress);
        const progress = Object.values(userProgress);
        const winners = Object.values(winnerList);
        const createArray = _.curry(helper.createRunnerUpsArray);
        let runnerUps = createArray(players)(progress);
        runnerUps = helper.sortWinnersArray(runnerUps);
        switch (players.length + winners.length) {
            case 0:
                phrase = phrase + `немає нікого, адже гравці так і не розпочали перегони. Шкода, ми розраховували побачити справжнє видовище!`;
                break;
            case 1:
                phrase = phrase + `${runnerUps[0].user}, але до фінішної прямої, на жаль, він не дійшов.`;
                break;
            case 2:
                if(runnerUps.length === 1) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${runnerUps[0].user}, але до фінішної прямої, на жаль, він не дійшов. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 2) {
                    phrase = phrase + `${runnerUps[0].user}. Другим йде ${runnerUps[1].user}, але до фінішної прямої, на жаль, жоден з них не дійшов. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                break;
            case 3:
                if(runnerUps.length === 1) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${winners[1][0]}. Його час становить ${winners[1][1]} сек. Останнім у трійці лідерів є ${runnerUps[0].user}, але до фінішної прямої, на жаль, він не дійшов. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 2) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${runnerUps[0].user}, третім - ${runnerUps[1].user} але до фінішної прямої, на жаль, вони не дійшли. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 3) {
                    phrase = phrase + `${runnerUps[0].user}. Другим йде ${runnerUps[1].user}, третім - ${runnerUps[2].user}, але до фінішної прямої, на жаль, вони не дійшли. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                break;
            case 4:
                if(runnerUps.length === 1) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${winners[1][0]}. Його час становить ${winners[1][1]} сек. Останнім у трійці лідерів є ${winners[2][0]} з часом ${winners[2][1]}. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 2) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${winners[1][0]}. Його час становить ${winners[1][1]} сек. Останнім у трійці лідерів є ${runnerUps[0].user}, але до фінішної прямої, на жаль, він не дійшов. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 3) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${runnerUps[0].user}, третім - ${runnerUps[1].user} але до фінішної прямої, на жаль, вони не дійшли. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 4) {
                    phrase = phrase + `${runnerUps[0].user}. Другим йде ${runnerUps[1].user}, третім - ${runnerUps[2].user}, але до фінішної прямої, на жаль, вони не дійшли. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                break;
            case 5:
                if(runnerUps.length === 1 || runnerUps.length === 2) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${winners[1][0]}. Його час становить ${winners[1][1]} сек. Останнім у трійці лідерів є ${winners[2][0]} з часом ${winners[2][1]}. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 3) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${winners[1][0]}. Його час становить ${winners[1][1]} сек. Останнім у трійці лідерів є ${runnerUps[0].user}, але до фінішної прямої, на жаль, він не дійшов. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 4) {
                    phrase = phrase + `${winners[0][0]}. Його час становить ${winners[0][1]} сек. Другим йде ${runnerUps[0].user}, третім - ${runnerUps[1].user} але до фінішної прямої, на жаль, вони не дійшли. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                if(runnerUps.length === 5) {
                    phrase = phrase + `${runnerUps[0].user}. Другим йде ${runnerUps[1].user}, третім - ${runnerUps[2].user}, але до фінішної прямої, на жаль, вони не дійшли. Подякуємо учасникам за неймовірно цікаве змагання!`;
                }
                break;
        }
        return phrase;
      }
}