export class Helper {
    // Higher order function
    sortWinnersArray(array) {
        return array.sort((a, b) => (a.score > b.score) ? -1 : ((b.score > a.score) ? 1 : 0));
    }

    sortWinners() {
        return (a, b) => a - b;
    }

    createRunnerUpsArray(players, progress) {
        const runnerUps = [];
        for (let i = 0; i < players.length; i++) {
            runnerUps.push({
            user: players[i],
            score: progress[i]
            });
        }
        return runnerUps;
    }

    createRandomNum(max) {
        return Math.floor(Math.random() * max);
    }
}