import { Helper } from "../helper/helper";
import { bot_lines } from "../../../bot-lines";

export class Random {
  constructor () {
    this.helper = new Helper()
    this.line = bot_lines.RANDOM[this.helper.createRandomNum(bot_lines.RANDOM.length)]
  }
}