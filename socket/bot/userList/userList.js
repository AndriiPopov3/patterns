import { bot_lines } from "../../../bot-lines";

export class UserList {
  constructor () {
    this.line = bot_lines.USER_LIST
  }

  buildUserListLine (userList) {
    let phrase = this.line;
    for (let i = 0; i < userList.length; i++) {
      phrase = phrase + `${bot_lines.RACE_NUMS[i]} виступає гонщик на ім'я ${userList[i]} на ${bot_lines.CAR_COLORS[i]} ${bot_lines.CARS[i]}. `
      if (i===2) {
        phrase = phrase + `${bot_lines.RACER_COMMENTS[0] }`
      }
    }
    if(userList.length === 1) {
      phrase = phrase + "Більше учасників сьогодні немає, отже, ми маємо можливість зосередитися на майстерності нашого єдиного спортсмена.";
    }
    return phrase;
  }
}