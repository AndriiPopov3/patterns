import * as _ from 'lodash';
import * as config from "./config";
import { users } from "./users";
import { texts } from "../data";
import { Bot } from "./bot";
import { mapper } from "./helpers/username-mapper";

const rooms = [];
const rooms_nums = {};


export default io => {
    io.on("connection", socket => {
        socket.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
        let currentRoomId;
        let username = users.get(socket.id.slice(6));
        let winnerList = {};
        let gameUserProgress = {};
        let onCloseFinal = 0;
        let onFinishLine = 0;
        let isRandomOn = true;
        const mapperCurry = _.curry(mapper);
        const mapperUsers = mapperCurry(users);

        const bot_final_results = function (winners) {
            const bot = new Bot();
            const comment = bot.createComment("final-results");
            const line = comment.buildFinalResultsLine(winners);
            return line;
        }

        const bot_on_close_final = function (gameUserProgress) {
            onCloseFinal = onCloseFinal + 1;
            if (onCloseFinal > 1) {
                return null;
            } else {
                const bot = new Bot();
                const comment = bot.createComment("close-final");
                const line = comment.buildCloseFinalLine(gameUserProgress);
                return line;
            }
        }

        const bot_on_finish_line = function (gameUserProgress) {
            onFinishLine = onFinishLine + 1;
            if (onFinishLine > 1) {
                return null;
            } else {
                const bot = new Bot();
                const comment = bot.createComment("finish-line");
                const line = comment.buildFinishLineLine(gameUserProgress);
                return line;
            }
        }

        socket.on("USERNAME_UNIQUE", user => {
            let counter = 0;
            for (let [k, v] of users) {
                if (v === user) {
                    counter = counter + 1;
                    if(counter === 2) {
                        users.delete(user);
                        socket.emit("USERNAME_EXISTS", user);
                        break;
                    } 
                }
            } 
        });

        socket.on("CREATE_ROOM", room_name => {
            if (rooms.indexOf(room_name) === -1) { // room name validation
                socket.join(room_name, () => {
                    rooms.push(room_name);
                    io.in(room_name).clients((error, clients) => {
                        if (error) throw error; 
                        rooms_nums[room_name] = clients.length;
                        currentRoomId = room_name;
                        socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
                        const userList = mapperUsers(clients);
                        socket.emit("CREATE_ROOM_INTERFACE", {room_name, userList, username});
                    });
                });
            } else {
                socket.emit("ROOM_NAME_EXISTS", room_name);
            }
        });

        socket.on("JOIN_ROOM", room_name => {
            if (rooms_nums[room_name] === config.MAXIMUM_USERS_FOR_ONE_ROOM) {
                socket.emit("ROOM_HAS_MAX_USERS", room_name);
            } else {
                socket.join(room_name, () => {
                    rooms_nums[room_name] = rooms_nums[room_name] + 1;
                    io.in(room_name).clients((error, clients) => {
                        if (error) throw error; 
                        currentRoomId = room_name;
                        const userList = mapperUsers(clients);
                        socket.emit("CREATE_ROOM_INTERFACE", {room_name, userList, username});
                        socket.to(room_name).emit("UPDATE_ROOM_INTERFACE", {room_name, userList, username});
                        
                    });
                    socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
                });
            }
        });

        socket.on("LEAVE_ROOM", room_name => {
            socket.leave(room_name, () => {
                rooms_nums[room_name] = rooms_nums[room_name] - 1;
                if (rooms_nums[room_name] === 0) {
                    rooms.splice(rooms.indexOf(room_name), 1);
                    delete rooms_nums[room_name];
                }
                currentRoomId = "";
                io.in(room_name).clients((error, clients) => {
                    if (error) throw error; 
                    const userList = mapperUsers(clients);
                    socket.to(room_name).emit("UPDATE_ROOM_INTERFACE", {room_name, userList, username});
                });
                socket.emit("LEAVE_ROOM_DONE", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
                socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
            });
        });

        socket.on("USER_READY", room_user => {
            const room = room_user.room;
            const user = room_user.user;
            io.in(room).clients((error, clients) => {
                if (error) throw error; 
                const roomUsersNum = clients.length;
                socket.emit("USER_READY_DONE", {room, user});
                socket.to(room_user.room).emit("USER_READY_DONE_ROOM", {room, user});
                socket.emit("ARE_USERS_READY", {room, user, roomUsersNum});
            });
        });

        socket.on("USER_NOT_READY", room_user => {
            const room = room_user.room;
            const user = room_user.user;;
            socket.emit("USER_NOT_READY_DONE", {room, user});
            socket.to(room_user.room).emit("USER_NOT_READY_DONE_ROOM", {room, user});
        });

        socket.on("START_COUNTDOWN", room => {
            rooms.splice(rooms.indexOf(room.room), 1);
            delete rooms_nums[room.room];
            socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
            const textId = Math.floor(Math.random() * texts.length);
            socket.emit("START_COUNTDOWN_DONE", {
                time: config.SECONDS_TIMER_BEFORE_START_GAME, 
                gameTime: config.SECONDS_FOR_GAME, 
                text: textId, 
                room: room.room
            });
            socket.to(room.room).emit("START_COUNTDOWN_DONE", {
                time: config.SECONDS_TIMER_BEFORE_START_GAME, 
                gameTime: config.SECONDS_FOR_GAME, 
                text: textId, 
                room: room.room
            });
        });

        socket.on("START_GAME", room_data => {
            socket.emit("START_GAME_DONE", {
                time: room_data.time, 
                gameTime: room_data.gameTime, 
                text: room_data.text, 
                room: room_data.room,
                user: username
            });
        });

        socket.on("UPDATE_PROGRESS_BARS", room_user => {  
            socket.emit("UPDATE_PROGRESS_BARS_DONE", {
                user: room_user.user,
                current: room_user.current,
                full: room_user.full,
                room: room_user.room,
                time: room_user.time
            });
            socket.to(room_user.room).emit("UPDATE_PROGRESS_BARS_DONE", {
                user: room_user.user,
                current: room_user.current,
                full: room_user.full,
                room: room_user.room,
                time: room_user.time
            });
        });

        socket.on("WRITE_USER_PROGRESS", data => {
            if (data.current / data.full !== 1) {
                gameUserProgress[data.user] = data.current / data.full;
            }
            if (data.full - data.current === 30) {
                const line = bot_on_close_final(gameUserProgress);
                if (line) {
                    socket.emit("BOT_SEND_MESSAGE", line);
                    socket.to(data.room).emit("BOT_SEND_MESSAGE", line);
                }
            }
            if (data.full - data.current === 10) {
                const line = bot_on_finish_line(gameUserProgress);
                if (line) {
                    socket.emit("BOT_SEND_MESSAGE", line);
                    socket.to(data.room).emit("BOT_SEND_MESSAGE", line);
                }
            }
        })

        socket.on("ADD_FINISHER", user_win => {
            winnerList[user_win.rank] = [user_win.user, user_win.time];
            delete gameUserProgress[user_win.user];
            io.in(user_win.room).clients((error, clients) => {
                if (error) throw error; 
                if (clients.length === Object.keys(winnerList).length) {
                    isRandomOn = false;
                    const userList = mapperUsers(clients);
                    for (let i = 0; i < Object.values(winnerList).length; i++) {
                        if (!userList.includes(Object.values(winnerList)[i][0])) {
                            const winnerRank = Object.keys(winnerList).find(key => winnerList[key] === Object.values(winnerList)[i][0]);
                            delete winnerList[winnerRank];
                        }
                    }
                    const line = bot_final_results(winnerList);
                    socket.emit("BOT_SEND_MESSAGE", line);
                    socket.to(user_win.room).emit("BOT_SEND_MESSAGE", line);
                    socket.emit("FINISH_GAME_DONE");
                }
            });
        });

        socket.on("GAME_TIMEOUT", user_room => { // doesn't handle disconnects that well yet
            isRandomOn = false;
            io.in(user_room.room).clients((error, clients) => {
                if (error) throw error; 
                const userList = mapperUsers(clients);
                for (let i = 0; i < Object.values(winnerList).length; i++) {
                    if (!userList.includes(Object.values(winnerList)[i][0])) {
                        const winnerRank = Object.keys(winnerList).find(key => winnerList[key][0] === Object.values(winnerList)[i][0]);
                        delete winnerList[winnerRank];
                    }
                }
                for (let i = 0; i < gameUserProgress.length; i++) {
                    if (!userList.includes(Object.keys(gameUserProgress[i])[0])) {
                        gameUserProgress.splice(i, 1);
                    }
                }
                const bot = new Bot();
                const comment = bot.createComment("game-timeout");
                const line = comment.buildGameTimeoutLine(gameUserProgress, winnerList);
                socket.emit("BOT_SEND_MESSAGE", line);
                socket.to(user_room.room).emit("BOT_SEND_MESSAGE", line);
                socket.emit("TIMEOUT_GAME_DONE");
            });
        });

        socket.on("ACTIVATE_BOT", room => {
            const bot = new Bot();
            const line = bot.createComment("greeting").say();
            socket.emit("ACTIVATE_BOT_DONE");
            socket.to(room).emit("ACTIVATE_BOT_DONE");
            socket.emit("BOT_SEND_MESSAGE", line);
            socket.to(room).emit("BOT_SEND_MESSAGE", line);
        });

        socket.on("BOT_NAME_USERS", room => {
            io.in(room).clients((error, clients) => {
                if (error) throw error; 
                const userList = mapperUsers(clients);
                const bot = new Bot();
                const comment = bot.createComment("userList");
                const line = comment.buildUserListLine(userList);
                socket.emit("BOT_SEND_MESSAGE", line);
                socket.to(room).emit("BOT_SEND_MESSAGE", line);
            });
        })

        socket.on("BOT_RACE_START", room => {
            const bot = new Bot();
            const line = bot.createComment("raceStart").say();
            socket.emit("BOT_SEND_MESSAGE", line);
            socket.to(room).emit("BOT_SEND_MESSAGE", line);
        })

        socket.on("BOT_NAME_AFTER_START_LEADER", room => {
            const bot = new Bot();
            const comment = bot.createComment("after-start-leader");
            const line = comment.buildAfterStartLeaderLine(gameUserProgress);
            socket.emit("BOT_SEND_MESSAGE", line);
            socket.to(room).emit("BOT_SEND_MESSAGE", line);
        })

        socket.on("BOT_RANDOM_LINE", room => {
            const random_timer = function() {
                const bot = new Bot();
                const line = bot.createComment("random").say();
                if (isRandomOn === false) {
                    clearTimeout(x);
                } else {
                    socket.emit("BOT_SEND_MESSAGE", line);
                    socket.to(room).emit("BOT_SEND_MESSAGE", line);
                    setTimeout(random_timer, 5000); // to fire messages each 5 seconds after the first trigger
                }
            }
            const x = setTimeout(random_timer, 16000); // to wait until first messages are fired
        })

        socket.on("BOT_ANNOUNCE_STATE", data => {
            const timeToEnd = Math.floor(data.distance / 1000);
            const bot = new Bot();
            const comment = bot.createComment("announce-state");
            const line = comment.buildAnnounceStateLine(gameUserProgress, timeToEnd);
            socket.emit("BOT_SEND_MESSAGE", line);
            socket.to(data.room).emit("BOT_SEND_MESSAGE", line);
        })

        socket.on("BOT_ANNOUNCE_FINISHER", data => {
            const bot = new Bot();
            const comment = bot.createComment("announce-finisher");
            const line = comment.buildAnnounceFinisherLine(data.user, data.rank);
            socket.emit("BOT_SEND_MESSAGE", line);
            socket.to(data.room).emit("BOT_SEND_MESSAGE", line);
        })

        socket.on("disconnect", () => {
            console.log(`${socket.id} disconnected`);
            users.delete(socket.id.slice(6)); // removes user from map so updaters can do their work properly
            if(currentRoomId) {
                if(rooms_nums[currentRoomId]) {
                    rooms_nums[currentRoomId] = rooms_nums[currentRoomId] - 1;
                }
                if (rooms_nums[currentRoomId] === 0) {
                    rooms.splice(rooms.indexOf(currentRoomId), 1);
                    delete rooms_nums[currentRoomId];
                }
                winnerList = {};
            }
            socket.broadcast.emit("UPDATE_ROOMS", [Object.keys(rooms_nums), Object.values(rooms_nums)]);
        });
    });
};