import * as config from "./config";
import game from "./game";
import { users } from "./users";

export default io => {
  io.on("connection", socket => {
    const username = socket.handshake.query.username;
      users.set(socket.id, username);
  });
  game(io.of("/game"));
};
